#include <Arduino.h>
#include <Wire.h>

int c[10][10] = {
        {0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
        {0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
        {0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
        {0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
        {0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
        {0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
        {0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
        {0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
        {0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
        {0, 0, 0, 0, 0, 0, 0, 0, 0, 0}
};

int calculateAddress(int x, int y) {
    return ((x + y) * (x + y + 1)) / 2 + y;
}


void setup() {
    Wire.begin();
    Serial.begin(9600);
    Serial.println("Done setting MASTER up!");
    delay(500);
}

void loop() {
    Serial.println("One loop Done!");
    for (int i = 0; i < 10; i++) {
        for (int j = 0; j < 10; j++) {
            int a = Wire.requestFrom(calculateAddress(i, j), 4, false);
            //Serial.println("A: " + String(a));
            if (a == 0) continue;
            Serial.println(calculateAddress(i, j));
            while (Wire.available()) {
                int k = Wire.read();
                Serial.println("k: " + String(k));
                c[i][j] = k;
                if (k == 63) {
                    Serial.println(String(calculateAddress(i, j)) + " is making noise");
                }
            }

        }
    }
    delay(400);
}
